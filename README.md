# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and uses TailwindCss for styling. (https://github.com/tailwindlabs/tailwindcss)

## Run locally

1. Clone the repo

```sh
git clone https://gitlab.com/kleaSadiku/lava_chains
```

2. Install the dependencies

```sh
npm install
```

3. In the project directory, you can run the app:

```sh
npm start
```
Runs the app in the development mode.\

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\