import { useEffect, useState } from "react";
import { StargateClient } from "@cosmjs/stargate";
import { MsgRelayPayment } from "@lavanet/lava-sdk/bin/src/codec/pairing/tx";
import { chainsData } from "../constants/chainsData";
import LoadingSpinner from "../components/LoadingSpinner";
import { Tx } from "cosmjs-types/cosmos/tx/v1beta1/tx";
import "../styles/chains.css";

// model of a single chain
export interface ChainModel {
  name: string;
  chainId: string;
  relays: number;
}

const Chains = () => {
  const [loaded, setLoaded] = useState(true as boolean);
  const [chains, setChains] = useState([] as ChainModel[]);

  const getTotalRelays = (block) => {
    let totalRelays = 0;

    for (const blockTx of block.txs) {
      const decodedTx = Tx.decode(blockTx);
      const messages = decodedTx?.body?.messages || [];

      for (const msg of messages) {
        if (msg.typeUrl.includes("MsgRelayPayment")) {
          const message = MsgRelayPayment.decode(msg.value);
          const relays = message.relays || [];

          for (const relay of relays) {
            totalRelays += relay.relayNum.toNumber();
          }
        }
      }
    }

    return { total: totalRelays };
  };

  const onChainUpdate = (item: ChainModel) => {
    setChains((prevState: any[]) => {
      const old = prevState ? [...prevState] : []; // Create a shallow copy of prevState
      // avoid duplication from async state update
      const newArray = old.filter(
        (oldChain) => oldChain.chainId !== item.chainId
      );
      newArray.push(item);
      // sort chains based on number of relays
      const sortedArray = newArray.sort((a, b) => b.relays - a.relays);
      return sortedArray;
    });
    setLoaded(false);
  };

  const getChainRelays = async (chain: { name: string; apiUrl: string }) => {
    try {
      const client = await StargateClient.connect(chain.apiUrl);
      const latestHeight = await client.getHeight();
      const chainId = await client.getChainId();
      let relaysData: { total: number };
      let relaysPerChain = 0;

      // Get 20 latest blocks for chain
      for (let i = 0; i < 20; i++) {
        const blockData = await client.getBlock(latestHeight - i);
        relaysData = getTotalRelays(blockData);
        relaysPerChain += relaysData?.total;
      }

      onChainUpdate({
        name: chain.name,
        chainId,
        relays: relaysPerChain,
      });
    } catch {
      // handle connect error
      console.log("Couldn't connect with: ", chain.name);
    }
  };

  const fetchData = async () => {
    setLoaded(true);
    setChains([]);
    // call getChainRelays for every chain
    await chainsData.forEach((chain) => getChainRelays(chain));
  };

  useEffect(() => {
    fetchData(); // Initial fetch
  }, []);

  return (
    <div className="chains-container">
      <div className="flex justify-between items-center mb-5">
        <p className="font-bold">Top Chains by Number of Relays</p>
        <button
          type="button"
          onClick={() => fetchData()}
          className="bg-red-100 text-white px-3 py-2 rounded-2xl"
        >
          Update data
        </button>
      </div>
      {loaded ? (
        <LoadingSpinner />
      ) : (
        <div className="flex flex-col">
          <div className="p-1.5 w-full inline-block align-middle">
            <div className="overflow-hidden border rounded-xl">
              <table className="chains-table">
                <thead className="chain-table-head">
                  <tr>
                    <th scope="col" className="chain-header-items">
                      Chain Name
                    </th>
                    <th scope="col" className="chain-header-items">
                      Number of Relays
                    </th>
                  </tr>
                </thead>
                <tbody className="divide-y divide-gray-200">
                  {chains.length > 0 &&
                    chains.map((chain: ChainModel) => (
                      <tr key={chain.name}>
                        <td className="chain-table-data">{chain.name}</td>
                        <td className="chain-table-data">{chain.relays}</td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Chains;
