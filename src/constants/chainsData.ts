// list of the chains with their api url gotten from lavanet gateway
export const chainsData = [
  {
    name: "LAVA (LAV1)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/lav1/rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Cosmos Hub (COS5)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/cos5/rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Cosmos Hub Testnet (COS5T)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/cos5t/rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Juno (JUN1)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/jun1/rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Juno Testnet (JUNT1)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/junt1/rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Evmos Testnet (EVMOST)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/evmost/tendermint-rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Evmos Mainnet (EVMOS)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/evmos/tendermint-rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Osmosis Testnet (COS4)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/cos4/rpc-http/6d9199739419b40780699928993b8e85",
  },
  {
    name: "Osmosis (COS3)",
    apiUrl: "https://g.w.lavanet.xyz:443/gateway/cos3/rpc-http/6d9199739419b40780699928993b8e85",
  }
]