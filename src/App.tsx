import Chains from './pages/Chains';
import './App.css';
import logo from './assets/lava-logo.svg';

function App() {
  return (
    <div className="App">
      <img src={logo} alt='lava logo'/>
      <Chains />
    </div>
  );
}

export default App;
