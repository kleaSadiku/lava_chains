/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{tsx, scss, css}',
    './src/pages/*.{tsx, scss, css}',
  ],
  theme: {
    extend: {
      colors: {
				primary: '#05090F',
				dark: '#0E0E0E',
				white: '#FFF9F8',
				gray: {
					100: '#B9B9B9'
				},
        red: {
          100: '#FF3801',
        }
			},
    },
  },
  plugins: [],
}

